<?php

//Sample1
$entrevistados = [
    ["id" => 201, "nome" => "Daniel Santana Ribeiro", "status" => "NCO"],
    ["id" => 202, "nome" => "Marlene Mendes Santana", "status" => "ATI"],
    ["id" => 203, "nome" => "José Virgílio Ribeiro", "status" => "NCO"],
];

foreach ($entrevistados as $entrevistado) {
    print_r($entrevistado['nome']);
    echo "\n";
}

$veiculos = array();
$veiculos[] = array("Carros" => ["FIAT", "VOLKSWAGEM", "NISSAN"]);
$veiculos[] = array("Motos" => ["SUSUKI", "KAWASAKI", "HARLEY DAVIDSON"]);
print_r($veiculos);

foreach ($veiculos as $veiculo) {
    if (isset($veiculo["Carros"])) {
        foreach ($veiculo["Carros"] as $carro) {
            print_r($carro);
            echo "\n";
        }
    } else {
        foreach ($veiculo["Motos"] as $moto) {
            print_r($moto);
            echo "\n";
        }
    }
    echo "\n";
}
